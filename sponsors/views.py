from django.shortcuts import render

from .models import SponsorModel


def index(request):
    sponsors_by_tier = []
    for tier, name in SponsorModel.TIER_CHOICES:
        sponsors_by_tier.append({
            'name': name,
            'sponsors': SponsorModel.get_enabled(request).filter(tier=tier),
        })
    return render(request, 'sponsors/index.html', context={'sponsors_by_tier': sponsors_by_tier, })
