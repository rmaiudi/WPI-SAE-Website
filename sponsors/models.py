import os

from django.db import models

from images.models import validate_image_ext
from sae_globals.models import SAEBaseModel, b_n


class SponsorModel(SAEBaseModel):
    def upload_to(self, current_filename) -> str:
        """Function called to dynamically upload a file to a specific location on the file system."""
        return os.path.join('logos', current_filename)

    TIER_A = 'TA'
    TIER_B = 'TB'
    TIER_C = 'TC'
    TIER_D = 'TD'
    TIER_E = 'TE'
    TIER_CHOICES = (
        (TIER_A, 'Titanium'),
        (TIER_B, 'Platinum'),
        (TIER_C, 'Gold'),
        (TIER_D, 'Silver'),
        (TIER_E, 'Bronze'),
    )

    tier = models.CharField('Tier', max_length=2, choices=TIER_CHOICES, **b_n)

    name = models.CharField('Name', max_length=500)
    logo = models.ImageField('Logo', validators=[validate_image_ext, ], upload_to=upload_to, **b_n)
    is_current = models.BooleanField('Current Sponsor?', default=False)
    years = models.CharField('Years Sponsored', max_length=500, **b_n)
    url = models.URLField('URL', **b_n)
    description = models.TextField('Description', **b_n)

    slugified_field = 'name'

    @property
    def logo_url(self):
        return self.logo.url if self.logo else ""

    @classmethod
    def top_sponsors(cls):
        return cls.objects.filter(tier=cls.TIER_A)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Sponsor"

