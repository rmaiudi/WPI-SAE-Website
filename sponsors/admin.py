from django.contrib import admin

from .models import SponsorModel


class SponsorModelAdmin(admin.ModelAdmin):
    fields = ['enabled', 'name', 'tier', 'is_current', 'years', 'logo', 'url', 'description', ]
    list_filter = ['enabled', 'tier', 'is_current', ]
    list_display = ['enabled', 'name', 'tier', 'is_current', 'years', ]
    list_display_links = list_display


admin.site.register(SponsorModel, SponsorModelAdmin)
