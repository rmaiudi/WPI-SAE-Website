from django.template import Library

from images.models import ImageModel


register = Library()


@register.simple_tag
def image_role(role: str) -> str:
    try:
        return ImageModel.objects.get(role=role).file_url
    except:
        return ""


