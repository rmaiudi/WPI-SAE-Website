import os

from django import template
from django.conf import settings


register = template.Library()


@register.simple_tag
def sstatic(path):
    '''
    Returns absolute URL to static file with versioning.
    '''
    # Return normal path if on localhost so that dev tools can be mapped.
    if not settings.LIVE_SERVER:
        return f'{ settings.STATIC_URL }{ path }'

    full_path = os.path.join(settings.STATIC_ROOT, path)
    try:
        # Get file modification time.
        mtime = os.path.getmtime(full_path)
        return f'{ settings.STATIC_URL }{ path }?{ mtime }'
    except OSError:
        # Returns normal url if this file was not found in filesystem.
        return f'{ settings.STATIC_URL }{ path }'
