from django.apps import AppConfig


class SAEGlobalsConfig(AppConfig):
    name = 'sae_globals'
