from datetime import timedelta

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.core.handlers.wsgi import WSGIRequest
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.utils.text import slugify

from crequest.middleware import CrequestMiddleware as Crequest


b_n = {
    'blank': True,
    'null': True,
}


class SearchQueryset(models.QuerySet):
    def search(self, query):
        # https://docs.djangoproject.com/en/1.10/ref/contrib/postgres/search/
        # Build search vector
        search_q = SearchQuery(query)
        vector = None
        for field in self.model.search_fields:
            if type(field) == tuple:
                field_name = field[0]
                if len(field) < 2:
                    field_weight = 'B'
                else:
                    field_weight = field[1]
            else:
                field_name = field
                field_weight = 'B'
            if not vector:
                vector = SearchVector(field_name, weight=field_weight)
            else:
                vector += SearchVector(field_name, weight=field_weight)
        return self.annotate(rank=SearchRank(vector, search_q)).filter(rank__gte=0.1).order_by('-rank')


class SAEManager(models.Manager):
    def get_queryset(self):
        return SearchQueryset(model=self.model, using=self._db, hints=self._hints)

    def search(self, query) -> models.QuerySet:
        return self.get_queryset().search(query)

    def get_enabled(self, request: WSGIRequest=None) -> models.QuerySet:
        # Always want public models
        enabled_filter = models.Q(enabled=SAEBaseModel.PUBLIC)
        if not request:
            request = Crequest.get_request()
        if request and request.user.is_staff:
            # If staff, get the staff ones too.
            enabled_filter |= models.Q(enabled=SAEBaseModel.STAFF_ONLY)
        return self.get_queryset().filter(enabled_filter)


class SAEBaseModel(models.Model):
    PUBLIC = 'PB'
    STAFF_ONLY = 'SO'
    NONE = 'NO'
    ENABLED_CHOICES = (
        (PUBLIC, 'Public'),
        (STAFF_ONLY, 'Staff Only'),
        (NONE, 'None'),
    )

    # Should this model be displayed?
    enabled = models.CharField('Enabled', max_length=2, default=PUBLIC, choices=ENABLED_CHOICES)
    # When was this model originally published? Can be modified, useful for putting dates further in the past.
    pub_date = models.DateTimeField('Date Published', default=timezone.now)
    # When was this model last updated? Automatically set.
    last_update = models.DateTimeField('Lasted Updated', auto_now=True, auto_now_add=False)
    # User to create
    created_by = models.ForeignKey(User, related_name='%(app_label)s_%(class)s_created', **b_n)
    # Last user to make a change
    last_edited_by = models.ForeignKey(User, related_name='%(app_label)s_%(class)s_last_edited', **b_n)

    slug = models.CharField('Slug', max_length=500, **b_n)

    objects = SAEManager()

    slugified_field = None

    def save(self, *args, **kwargs):
        request = Crequest.get_request()
        if not self.pk:
            self.created_by = request.user
        self.last_edited_by = request.user
        if self.slugified_field:
            self.slug = slugify(getattr(self, self.slugified_field))
        super(SAEBaseModel, self).save(*args, **kwargs)

    def was_published_recently(self) -> bool:
        """
        Checks if current model was published within the past 180 days. Models published in the future return false.

        :rtype: bool
        """
        now = timezone.now()
        return now - timedelta(days=180) <= self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Recent?'

    def is_new(self) -> bool:
        """
        Proxy for checking if a model is new. Allows for easy swapping of published vs updated for determining if
        something should be marked as new.

        :rtype: bool
        """
        return self.was_published_recently()

    def was_updated_recently(self) -> bool:
        """
        Same as `was_published_recently()` except it checks last_update instead of pub_date.

        :return: bol
        """
        now = timezone.now()
        return now - timedelta(days=180) <= self.last_update <= now

    @classmethod
    def get_enabled(cls, request=None):
        return cls.objects.get_enabled(request)

    was_updated_recently.admin_order_field = 'last_update'
    was_updated_recently.boolean = True
    was_updated_recently.short_description = 'Recent?'

    class Meta:
        abstract = True


class AbstractPostModel(SAEBaseModel):
    title = models.CharField('Title', max_length=500)
    content = models.TextField('Content')
    image = models.ForeignKey('images.ImageModel', **b_n)
    slugified_field = 'title'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(AbstractPostModel, self).save(*args, **kwargs)

    @property
    def image_tn_url(self):
        return self.image.tn_url if self.image else ''

    @property
    def image_file_url(self):
        return self.image.file_url if self.image else ''

    @property
    def image_title(self):
        return self.image.title if self.image else self.title

    def abs_url(self):
        return ''

    def __str__(self):
        return self.title

    class Meta:
        abstract = True
