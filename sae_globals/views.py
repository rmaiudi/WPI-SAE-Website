from django.core.mail import EmailMessage
from django.shortcuts import render, reverse
from django.template import Context
from django.template.loader import get_template
from django.conf import settings
from django.http import HttpResponseRedirect

from sponsors.models import SponsorModel

from .forms import ContactForm


def home(request):
    return render(request, 'sae_globals/home.html', {'top_sponsors': SponsorModel.top_sponsors(), })


def contact(request):
    if request.method == 'POST':
        form = ContactForm(data=request.POST)

        if form.is_valid():
            contact_name = form.cleaned_data.get('contact_name', '')

            contact_email = form.cleaned_data.get('contact_email', '')
            form_content = form.cleaned_data.get('content', '')

            # Email the profile with the contact information
            template = get_template('sae_globals/contact_email.html')
            context = Context({
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            })
            content = template.render(context)

            email = EmailMessage(
                "New contact form submission",
                content,
                "Your website" + '',
                [settings.DEFAULT_FROM_EMAIL, ],
                headers={'Reply-To': contact_email, }
            )
            email.send()
            return HttpResponseRedirect(reverse('sae_globals:contact') + "?success=True")
    else:
        form = ContactForm()

    return render(request, 'sae_globals/contact.html', {'form': form, })
