import re
from collections import defaultdict
from time import strptime

from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import NewsPostModel


def filter_by_month_year(request, objs):
    date = request.GET.get('date', None)
    if date:
        match = re.match('(?P<month>\w+)-(?P<year>\d+)', date)
        if match:
            try:
                month_str = match.group('month')
                month = strptime(month_str, '%B').tm_mon
                year = match.group('year')
            except ValueError:
                # If the month is not valid, this clause will run. Simply ignore it and allow the function to return all
                pass
            else:
                filtered_month = "%s %s" % (month_str, year, )
                return objs.filter(pub_date__year__gte=year, pub_date__month__gte=month,
                                   pub_date__year__lte=year, pub_date__month__lte=month), filtered_month
    # If we get to here, a date wasn't valid or provided, so just return all
    return objs, False


def index(request):
    news_posts = NewsPostModel.get_enabled(request).order_by('-pub_date')

    dates = defaultdict(int)
    time_str = "%s %d"
    for p in news_posts:
        dates[time_str % (p.pub_date.strftime('%B'), p.pub_date.year,)] += 1

    news_posts, filtered_month = filter_by_month_year(request, news_posts)
    if filtered_month:
        subtitle = filtered_month.capitalize()
    else:
        subtitle = None

    paginator = Paginator(news_posts, 10)  # Show 10 posts per page
    try:
        news_posts = paginator.page(request.GET.get('page', 1))
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        news_posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        news_posts = paginator.page(paginator.num_pages)

    return render(request, 'news/index.html', {'posts': news_posts, 'dates': dict(dates), 'subtitle': subtitle, })


def post(request, slug, pk):
    news_post = get_object_or_404(NewsPostModel.get_enabled(request), pk=pk)
    return render(request, 'news/post.html', {'post': news_post, })
