from django.contrib import admin

from .models import NewsPostModel


class NewsPostModelAdmin(admin.ModelAdmin):
    fields = ['enabled', 'title', 'content', 'image', ]


admin.site.register(NewsPostModel, NewsPostModelAdmin)
