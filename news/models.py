from django.shortcuts import reverse

from sae_globals.models import AbstractPostModel


class NewsPostModel(AbstractPostModel):

    def abs_url(self):
        return reverse('news:post', kwargs={'pk': self.pk, 'slug': self.slug, })

    class Meta:
        verbose_name = "News Post"

