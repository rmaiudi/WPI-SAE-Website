from django.contrib import admin

from .models import ProjectModel


class ProjectModelAdmin(admin.ModelAdmin):
    fields = ['enabled', 'title', 'content', 'image', ]


admin.site.register(ProjectModel, ProjectModelAdmin)
