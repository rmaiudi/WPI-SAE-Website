from django.shortcuts import reverse

from sae_globals.models import AbstractPostModel, b_n


class ProjectModel(AbstractPostModel):

    def abs_url(self):
        return reverse('projects:project', kwargs={'pk': self.pk, 'slug': self.slug, })

    class Meta:
        verbose_name = "Project"

