from django.shortcuts import render, get_object_or_404

from .models import ProjectModel


def index(request):
    projects = ProjectModel.get_enabled(request).order_by('-pub_date')
    return render(request, 'projects/index.html', {'projects': projects, })


def project(request, slug, pk):
    project_obj = get_object_or_404(ProjectModel.get_enabled(request), pk=pk)
    return render(request, 'projects/project.html', {'project': project_obj, })

