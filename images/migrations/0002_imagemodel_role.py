# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-06 20:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('images', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='imagemodel',
            name='role',
            field=models.CharField(blank=True, choices=[('HP', 'Home Page')], max_length=2, null=True, verbose_name='Image Role'),
        ),
    ]
