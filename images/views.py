from collections import defaultdict

from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q

from news.views import filter_by_month_year

from .models import ImageModel


def index(request):
    images = ImageModel.get_enabled(request).filter(Q(role='') | Q(role=None)).order_by('-pub_date')

    dates = defaultdict(int)
    time_str = "%s %d"
    for p in images:
        dates[time_str % (p.pub_date.strftime('%B'), p.pub_date.year,)] += 1

    images, filtered_month = filter_by_month_year(request, images)
    if filtered_month:
        subtitle = filtered_month.capitalize()
    else:
        subtitle = None

    paginator = Paginator(images, 25)  # Show 25 posts per page
    try:
        images = paginator.page(request.GET.get('page', 1))
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        images = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        images = paginator.page(paginator.num_pages)

    return render(request, 'images/index.html', {'images': images, 'dates': dict(dates), 'subtitle': subtitle, })


def image(request, slug, pk):
    image_obj = get_object_or_404(ImageModel.get_enabled(request), pk=pk)
    return render(request, 'images/image.html', {'image': image_obj, })

