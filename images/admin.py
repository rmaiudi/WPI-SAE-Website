from django.contrib import admin

from .models import ImageModel


class ImageModelAdmin(admin.ModelAdmin):

    def add_view(self, request, form_url='', extra_context=None):
        self.fields = ['enabled', 'title', 'file', 'role', ]
        self.readonly_fields = ()
        return super(ImageModelAdmin, self).add_view(request, form_url, extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.fields = ['enabled', 'title', 'file', 'role', 'file_url', ]
        self.readonly_fields = ['file_url', ]
        return super(ImageModelAdmin, self).change_view(request, object_id, form_url, extra_context)


admin.site.register(ImageModel, ImageModelAdmin)
