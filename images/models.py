import hashlib
import os

from PIL import Image
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.text import slugify

from sae_globals.models import SAEBaseModel, b_n


def validate_image_ext(value):
    """Make sure that uploaded image files only have extensions found in the list below."""
    image_file_exts = ['.jpg', '.jpeg', '.png', ]
    ext = os.path.splitext(value.name)[1].lower()
    if image_file_exts and ext not in image_file_exts:
        if len(image_file_exts) >= 3:
            allowed_str = ', '.join(image_file_exts[:-1]) + ', and ' + image_file_exts[-1]
        else:
            allowed_str = ' and '.join(image_file_exts)
        raise ValidationError("This file is not the right type. Only {} files are allowed.".format(allowed_str))


class ImageModel(SAEBaseModel):
    def upload_to(self, current_filename) -> str:
        """Function called to dynamically upload a file to a specific location on the file system."""
        return os.path.join('images', current_filename)

    title = models.CharField('Title', max_length=500)

    file = models.ImageField('File', validators=[validate_image_ext, ], upload_to=upload_to, **b_n)
    file_hash = models.CharField('File Hash', max_length=500, **b_n)

    tn = models.ImageField('Thumbnail', **b_n)

    ROLE_HOME_PAGE = 'HP'
    ROLE_NEWS = 'NW'
    ROLE_PROJECTS = 'PR'
    ROLE_IMAGES = 'GA'
    ROLE_SPONSORS = 'SP'
    ROLE_CONTACT = 'CT'
    ROLE_CHOICES = (
        (ROLE_HOME_PAGE, 'Home Page'),
        (ROLE_NEWS, 'News Page'),
        (ROLE_PROJECTS, 'Projects Page'),
        (ROLE_IMAGES, 'Gallery Page'),
        (ROLE_SPONSORS, 'Sponsors Page'),
        (ROLE_CONTACT, 'Contact Page'),
    )
    role = models.CharField('Image Role', max_length=2, choices=ROLE_CHOICES, **b_n)

    slugified_field = 'title'

    def save(self, *args, **kwargs):
        # Save this model directly so that the file gets uploaded.
        self.slug = slugify(self.title)
        super(ImageModel, self).save(*args, **kwargs)

        # Create thumbnail if file has changed
        if self.file:
            new_file_hash = self._create_file_hash(self.file.file)
            if self.file_hash != new_file_hash:
                self._make_tn()
                self.file_hash = new_file_hash

        # If another image shares the same role as this image, remove theirs.
        if self.role:
            same_role_images = ImageModel.objects.filter(role=self.role).exclude(pk=self.pk)
            for i in same_role_images:
                i.role = None
                i.save()

        models.Model.save(self)

    def _make_tn(self):
        TN_SIZE = (600, 0xFFFFFFFF)
        """Create and save the thumbnail for the photo (simple resize with PIL)."""
        # Open the currently loaded file and then scale it.
        try:
            #                       LOLOLOLOL
            image = Image.open(self.file.file.file)
        except FileNotFoundError:
            # Good chance we are moving files around.
            return
        except ValueError:
            # fs doesn't have a file associated with it.
            return

        ftype = image.format
        try:
            image.thumbnail(TN_SIZE, Image.ANTIALIAS)
        except OSError:
            # TODO: Either log error or use gimp cli to fix image.
            error = True
        else:
            error = False

        # Get filename and add file_id to it.
        temp = self.file.file.name.lstrip(settings.MEDIA_ROOT).split('.')
        temp.insert(-1, 'tn')
        tn_filename = ".".join(temp)

        # Save the tn to a real file via a ContentFile
        abs_tn_filepath = os.path.join(settings.MEDIA_ROOT, tn_filename)
        os.makedirs(os.path.dirname(abs_tn_filepath), exist_ok=True)
        with open(abs_tn_filepath, 'wb') as tn_out:
            if not error:
                image.save(tn_out, ftype)

        self.tn = tn_filename

    @staticmethod
    def _create_file_hash(file, buffer_size=65536):
        """Hash self.file and return the sha1 sum"""
        sha1 = hashlib.sha1()
        data = file.read(buffer_size)
        while data:
            sha1.update(data)
            data = file.read(buffer_size)
        return sha1.hexdigest()

    @property
    def tn_url(self):
        return self.tn.url if self.tn else self.file_url

    @property
    def file_url(self):
        return self.file.url if self.file else ''

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Image"
